# Emojis by gardenbot

## ms_robot_bee.png
![](gardenbot/ms_robot_bee.png)

## ms_robot_seedling.png
![](gardenbot/ms_robot_seedling.png)

## ms_robot_sunflower.png
![](gardenbot/ms_robot_sunflower.png)

## ms_robot_thinkthonk.png
![](gardenbot/ms_robot_thinkthonk.png)

## ms_robot_upside_down_display.png
![](gardenbot/ms_robot_upside_down_display.png)

